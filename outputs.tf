output "public_dns" {
  value = module.aws_ec2_instance.ec2_public_dns
}

output "public_ip" {
  value = module.aws_ec2_instance.ec2_public_ip
}

output "primary_ssh_string" {
  description = "Copy paste this string to SSH into the primary."
  value       = module.aws_ec2_instance.ec2_primary_ssh_string
}
