
//--------------------------------------------------------------------
// Modules
data "terraform_remote_state" "vpc" {
  backend = "remote"

  config = {
    organization = "beaulabs"
    workspaces = {
      name = "nonprod-1-aws-vpc-sg"
    }
  }
}

module "aws_ec2_instance" {
  source  = "app.terraform.io/beaulabs/aws-ec2-instance/aws"
  version = "1.1.0"

  #security_group_ids = module.aws_security_group.security_group_id
  security_group_ids = [data.terraform_remote_state.vpc.outputs.nonprod_new_security_group_id_ssh, data.terraform_remote_state.vpc.outputs.nonprod_new_security_group_id_web]
  vpc_subnet_id      = data.terraform_remote_state.vpc.outputs.nonprod_new_vpc_subnet_id
  server_name        = "beau25mar"
}
